import sys
from model.ElementaryCellularAutomaton import ElementaryCellularAutomaton as ECA
from model.LifeGame import LifeGame,NEUMANN,MOORE
from view.RuleAutomaton256View import RuleAutomaton256View
from view.LifeGameView import LifeGameView

def test_elementary(argv):
    if len(argv) == 0:
        rule = None
    else:
        rule = int(argv[0])
    width = 201 #default value
    iterations = 100 # default value
    for arg in argv:
        ##arg is key=value
        try:
            exec(arg[2:])
        except:
            print(arg+" is not a command ")
    state_colors = ((255,255,255),(255,0,0))
    print("rule: ",rule)
    m = ECA(rule,width=width,iterations=iterations)
    v = RuleAutomaton256View(m,state_colors)
    m.addView(v)
    m.start()


def test_locator():
    l = Locator((5,5))
    pos = (2,6)
    linear_pos = l.linearpos(pos)
    realpos = l.pos(linear_pos)
    print("pos: ",pos)
    print("linearpos: ",linear_pos)
    print("realpos: ",realpos)
    '''
    0 0 0 0 0
    0 0 0 0 0
    0 0 1 0 0
    0 0 0 1 0
    0 0 0 0 0
    '''

def getRules(game):
    dead = 0
    live = 1
    if game == "PROFE":
        rules = {
            dead: [lambda neighbors:  live if neighbors[live] in [2,3] else dead],
            live: [lambda neighbors:  live if neighbors[live] in [1,2] else dead]
        }
    elif game == "GAME_LIFE":
        rules = {
            dead: [lambda neighbors:  live if neighbors[live] == 3  else dead],
            live: [lambda neighbors:  live if neighbors[live] in [2,3] else dead]
        }
    return rules

def game_life(argv):
    type_automata =argv[0]
    rule=None
    if len(argv) <3:
        dimensions = None
    else:
        dimensions = (int(argv[1]),int(argv[2]))
    occupancy = None
    mode= NEUMANN
    for arg in argv[3:]:
        ##arg is key=value
        try:
            exec(arg[2:])
        except:
            print(arg+" is not a command ")

    if occupancy is not None and (occupancy>1  or occupancy<=0):
        occupancy = 0.2
        print("invalid occupancy, new ocupancy = ",occupancy)
    dead = 0
    live = 1
    states = [dead,live]
    rules = getRules(type_automata)



    game = LifeGame(states,rules,neighbor_mode=mode,occupancy=occupancy,dimensions=dimensions)
    state_colors = ((0,0,0),(255,255,255))
    view = LifeGameView(game,state_colors)
    game.addView(view)
    game.start()

def exit_failure():
    sys.exit(("USAGE 256 RULE: {0} 256R rule \n"+
                "\t --width= number of cells\n"+
                "\t --iterations= number of iterations\n"+
              "USAGE GAME_LIFE|PROFE: {0} GAME_LIFE|PROFE  width height\n"+
                "\t --neighbor= NEUMANN | MOORE, default is NEWMAN\n"+
                "\t --occupancy= for create initial generation.\n"
                          ).format(sys.argv[0]))
def main():
    argv = sys.argv
    if len(sys.argv) ==1:
        exit_failure()
    if sys.argv[1] == "256R":
        test =  test_elementary
        argv=argv[2:]
    elif sys.argv[1] in ["GAME_LIFE","PROFE"]:
        test = game_life
        argv=argv[1:]
    else:
        exit_failure()
    test(argv)

#test_elementary()
if __name__ == "__main__":
    main()
