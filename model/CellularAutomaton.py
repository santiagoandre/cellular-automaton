import fileinput
from core.MVC import Model
from random import randint as rand
import math
class CellularAutomaton(Model):
	def __init__(self,iterations=100):
		Model.__init__(self)
		self._generation = 0

		if iterations!=None and iterations<=0:
			iterations = 40
		self._iterations= iterations
		self._done= False
	def iterations(self):
		return self._iterations
	def generations(self):
		return self._generation

	def start(self):
		self.init_generation()
		while not self.isdone():
			self.notify()
			self.next_generation()
			self._generation+=1
		print("done")
		self.notify()
	def isdone(self):
		return  (self._iterations!= None and self._generation>= self._iterations-1)
