import math
import random
from .CellularAutomaton import CellularAutomaton
NEUMANN = 0
MOORE = 1
class LifeGame(CellularAutomaton):

	def __init__(self,states,rules,neighbor_mode,occupancy,dimensions=None,iterations=None):
		self._occupancy = occupancy
		CellularAutomaton.__init__(self,iterations)
		if dimensions is None:
			dimensions = (100,100)
		self._dimensions = dimensions
		self._states = states
		self._rules = rules
		if neighbor_mode not in [NEUMANN,MOORE]:
			self._neighbor_mode=  NEUMANN
		else:
			self._neighbor_mode=  neighbor_mode
		if occupancy is not None:
			print("occupancy: "+str(occupancy))
		if self._neighbor_mode == NEUMANN:
			print("NEUMANN neighbor")
		else:
			print("Moore neighbor")
	def width(self):
		return self._dimensions[0]
	def heigth(self):
		return self._dimensions[1]
	def board(self):
		return self._board
	def random_generation(self):
		print("Random init generation.")
		occupancy = self._occupancy
	# Instantiate the board as a dictionary with a fraction occupied
	# 0 indicates an empty cell; 1 indicates an occupied cell
		board = dict()
		for x in range(self.width()):
			for y in range(self.heigth()):
				if random.random() < occupancy:
					board[(x,y)] = self._states[random.randint(1,len(self._states)-1)]
				else:
					board[(x,y)] = self._states[0]
		self._board = board

	def paterin_init_generation(self):
		print("Pattern init generation.")
	# Instantiate the board as a dictionary with a fraction occupied
	# 0 indicates an empty cell; 1 indicates an occupied cell
		board = dict()
		for x in range(self.width()):
			for y in range(self.heigth()):
				board[(x,y)] = self._states[0]
		x = self.width()//2
		y = self.heigth()//2
		board[(x,y)] = self._states[random.randint(1,len(self._states)-1)]
		board[(x,y+1)] = self._states[random.randint(1,len(self._states)-1)]
		board[(x+1,y)] = self._states[random.randint(1,len(self._states)-1)]
		board[(x-1,y)] = self._states[random.randint(1,len(self._states)-1)]
		board[(x-1,y-1)] = self._states[random.randint(1,len(self._states)-1)]
		self._board = board
	def init_generation(self):
		if self._occupancy == None:
			self.paterin_init_generation()
		else:
			self.random_generation()
	def _update_cell(self,cell_pos):
		count_neighbors = self._count_neighbors(cell_pos)
		state= self._board[cell_pos]
		for rule in self._rules[state]:
			new_state = rule(count_neighbors)
			if new_state is not None:
				break
	#	print(cell_pos,": ",state," + ",count_neighbors," -> ",new_state)
		return new_state
	def _count_neighbors(self,cell_pos):#cuenta el numero de vecinos por estado
		count = {state:0 for state in self._states}
		x,y = cell_pos
		neighbors     =self._neighbors(x,y)
		for neighbor in neighbors:
			#frontera ciclica
			x,y = neighbor
			if x == -1:
				x = self.width()-1
			elif x == self.width():
				x = 0
			if y == -1:
				y = self.heigth()-1
			elif y == self.heigth():
				y = 0
			#conteo de los estados de su vecindad
			state_cell =  self._board[(x,y)]
			count[state_cell] += 1

		#print(cell_pos,": ",count)
		return count #retorno de conteo
	def _neighbors(self,x,y):
		if self._neighbor_mode == NEUMANN:
			return [      (x  , y-1),
					 (x-1 ,  y),            (x+1 ,  y),
								  (x  , y+1)
			]
		else:
			return  [  (x-1 , y-1),    (x  , y-1), (x+1  , y-1),
					 (x-1 ,  y),             (x+1 ,  y),
					(x-1 , y+1),	  (x  , y+1) , (x+1 , y+1)
			]
	def next_generation(self):
		board = {}
		for cell_pos in self._board:
			new_state = self._update_cell(cell_pos)
			if new_state is not None:
				board[cell_pos] = new_state
			else:
				board[cell_pos] = self._board[cell_pos]
			#print(self._board[cell_pos],", ",cell_pos,", ",new_state )
		#raw_input("next")
		self._board = board
