import math
from random import randint as rand
from .CellularAutomaton import CellularAutomaton
class ElementaryCellularAutomaton(CellularAutomaton):
	def __init__(self,rule=None,width = 201, iterations=100,len_parents=3):
		CellularAutomaton.__init__(self,iterations)
		self._later_generation=None
		if len_parents%2==0:
			self._len_parents= 3
		else:
			self._len_parents = len_parents
		self._width = int(width)
		self._persitence=  None
		self.set_rule(rule)
	def width(self):
		return self._width
	def later_generation(self):
		return self._later_generation
	def set_rule(self,new_rule):
		len_rules = int(math.pow(2,self._len_parents))
		num_rules = int(math.pow(2,len_rules))

		if new_rule == None or new_rule<=0 or new_rule>=num_rules:
			new_rule =  rand(1, num_rules-1)
		self._rules= bin(new_rule)[2:]
		left_zeros = len_rules - len(self._rules)
		self._rules = "0"*left_zeros+self._rules
		self._rules = self._rules[::-1]
		self._rules = [int(r) for r in self._rules]
		print("rule "+new_rule.__str__()+": "+self._rules.__str__())
	def init_generation(self):

		generation = [0 for _ in range(self._width)]
		generation[(self._width+1)//2] =1
		self._later_generation= generation
		self.save()
	def parent_pattern(self,cell_pos):
		parents = []

		radio = (self._len_parents-1)//2
		first_limit = cell_pos-radio
		end_limit = cell_pos+radio+1
		if first_limit<0:
			parents+= self._later_generation[first_limit:]
			first_limit = 0
		if end_limit> self._width:
			parents+= self._later_generation[: end_limit-self._width]
		parents+= self._later_generation[first_limit: end_limit]
		#patterns =  self._later_generation[first_limit:]+  self._later_generation[:end_limit]
		str_num = ''.join(map(str,parents))
		#print(len(patterns),end=", ")
		return int(str_num,2)
	def next_generation(self):
		self._later_generation =  [self._rules[self.parent_pattern(xi)] for xi in range(self._width) ]
		self.save()
	def save(self):
		self._persitence.write(' '.join(map(str,self._later_generation))+"\n")
	def start(self):
		self._persitence = open("out.txt","w")
		CellularAutomaton.start(self)
		self._persitence.close()
