from threading import Thread
import sys
import pygame

from pygame import QUIT
from core.MVC import View
class FunctionThread(Thread):
	def __init__ (self, f,args=[]):
		Thread.__init__(self)
		self.args = args
		self.f=f
	def run(self):
		self.f(*self.args)

class PygameView(View):
	def __init__(self,model,width=700,height=700):
		self._dimecions = width,height
		self.__initview__(model)
		self._display = pygame.display.set_mode(self.dimensions()) #se obtiene el display, entregando una dupla = (ancho, al
		FunctionThread(self.catch_events).start()

	def catch_events(self):
		pygame.init()
		while True:
			for event in pygame.event.get():
				if event.type == QUIT:
					self._quit_event()
				self._process_event(event)
	def _quit_event(self):
		print("bye.")
		pygame.quit()
		sys.exit()
	def _process_event(self,event):
		pass
	def set_width(self,width):
		self._dimecions = width,self._dimecions[1]
	def set_height(self,height):
		self._dimecions = self._dimecions[0],height
	def width(self):
		return self._dimecions[0]
	def height(self):
		return self._dimecions[1]
	def dimensions(self):
		return self._dimecions
	def display(self):
		return self._display
