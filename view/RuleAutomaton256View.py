import pygame
from .PygameView import PygameView

class RuleAutomaton256View(PygameView):
	def __init__(self,model,state_colors,width=700,height=700):
		PygameView.__init__(self,model,width,height)
		self._state_colors= state_colors
	def __initview__(self,model):
		if model.width()>=model.iterations():
			self._width_cell = self.width()//model.width()
		else:
			self._width_cell = self.height()//model.iterations()

		self.set_width(self._width_cell*(model.width()))
		self.set_height(self._width_cell*model.iterations())
	def update(self,model):
		generation = model.generations()
		for xi,cell in enumerate(model.later_generation()):
			self.draw_cell(cell,generation,xi)
		if model.isdone():
			print("update display")
			pygame.display.update()#actualizar el display
	def draw_cell(self,cell,column,row):
		posx= self._width_cell*row
		posy = self._width_cell*column
		pygame.draw.rect(self.display(),self._state_colors[cell],(posx,posy,self._width_cell,self._width_cell))
		#
