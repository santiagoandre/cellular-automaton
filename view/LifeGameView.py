
import pygame
from .PygameView import PygameView

class LifeGameView(PygameView):
	def __init__(self,model,state_colors,width=700,height=700):
		PygameView.__init__(self,model,width,height)
		self._states = state_colors
		self._model = model
	def __initview__(self,model):
		if model.width()>=model.heigth():
			self._width_cell = self.width()//model.width()
		else:
			self._width_cell = self.height()//model.heigth()

		self.set_width(self._width_cell*(model.width()))
		self.set_height(self._width_cell*model.heigth())
	def update(self,model):
		board = model.board()
		cell_width = self._width_cell
		 # Draw every cell in the board as a rectangle on the screen
		for cell_pos in board:
			x,y = cell_pos
			self.draw_cell(board[cell_pos],x,y)
		#print("update display")
		pygame.display.update()#actualizar el display
		if model.isdone():
			PygameView._quit_event(self)
	def draw_cell(self,cell,column,row):
		posx= self._width_cell*row
		posy = self._width_cell*column
		pygame.draw.rect(self.display(),self._states[cell],(posx,posy,self._width_cell,self._width_cell))
		#
