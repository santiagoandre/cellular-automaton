class Model:
    def __init__(self):
        self.views = []
    def addView(self,view):
        self.views.append(view)
    def notify(self):
        for view in self.views:
            view.update(self)

class View():
    def update(self,model):
        pass
